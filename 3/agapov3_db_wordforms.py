import lxml.etree as tree
import re
import sqlite3

INPUT_NAME = 'udmwiki'
OUTPUT_NAME = 'udmwiki_stats'
MAXSIZE = 100 # maximum size for frequency-storing python dictionary

dump = open(INPUT_NAME+'.xml', 'r', encoding='utf8').readlines()

output = sqlite3.connect(OUTPUT_NAME+'.db')
output.execute('CREATE TABLE articles (title text, links int, words int)')
output.execute('CREATE TABLE freqlist (word text, count int)')

page_start = re.compile('<page>')
page_end = re.compile('</page>')
re_links = re.compile('\[\[.+?\]\]')
re_words = re.compile('\w*')

freq_d = {}

def dump_to_db(dict):
	global freq_d
	for word in freq_d:
		output.execute('INSERT INTO freqlist VALUES (?,0) WHERE NOT EXISTS (SELECT 1 FROM freqlist WHERE word=?)', (word, word))
		output.execute('UPDATE TABLE freqlist SET count=count+? WHERE word=?', (word, freq_d[word]))
	freq_d = {}

current_page = ''
inside_page = 0
for line in dump:
	if re.search(page_start, line):
		inside_page = 1
		current_page = ''
	elif re.search(page_end, line):
		inside_page = 0
		current_page += '</page>'

		# page content loaded, do your stuff
		try:
			root = tree.fromstring(current_page)
			article_text = root[-1][-2].text

			title = root[0].text

			links = re.findall(re_links, article_text)
			words = re.findall(re_words, article_text)
			for i in range(len(words)):
				words[i] = words[i].lower()

			nlinks = len(links)
			nwords = len(words)

			# populate freq_d (line 19)
			for i in words:

				# if freq_d reached maxsize, push it to database and wipe
				if len(freq_d) == maxsize:
					dump_to_db(freq_d)

				if i in freq_d:
					freq_d[i] += 1
				else:
					freq_d[i] = 1

			output.execute('INSERT INTO articles VALUES (?,?,?)', (title, nlinks, nwords))
		except:
			pass

	dump_to_db(freq_d) # dump remainder from last '59 dump
	
	if inside_page == 1:
		current_page += (line)

output.commit()
output.close()