import lxml.etree as tree
import re

INPUT_NAME = 'udmwiki.xml'
OUTPUT_NAME = 'file.csv'

dump = open(INPUT_NAME, 'r', encoding='utf8').readlines()
outp = open(OUTPUT_NAME, 'w', encoding='utf8')

page_start = re.compile('<page>')
page_end = re.compile('</page>')
re_links = re.compile('\[\[.+?\]\]')
re_words = re.compile('\w*')

current_page = ''
inside_page = 0
for line in dump:
	if re.search(page_start, line):
		inside_page = 1
		current_page = ''
	elif re.search(page_end, line):
		inside_page = 0
		current_page += '</page>'

		# page content loaded, do your stuff
		try:
			root = tree.fromstring(current_page)
			text = root[-1][-2].text

			title = root[0].text
			links = len(re.findall(re_links, text))
			words = len(re.findall(re_words, text))

			outp.write('%s;%d;%d\n' % (title, links, words))
		except:
			pass

	if inside_page == 1:
		current_page += (line)

outp.close()